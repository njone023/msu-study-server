﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MSUStudyChatServer.Models;

namespace MSUStudyChat.Contracts
{
    public interface IChatRepository
    {
        //Task<bool> SaveUser(User user);
        Task SaveUser(User user);
        Task<bool> SaveMessages(string userId, List<Message> messages);
        Task<bool> SaveSurvey(string userId, SurveyData surveyData);
        Task<bool> SaveTimeElapsed(string userId, TimeElapsed timeElapsed);
    }
}
