﻿using MSUStudyChatServer.Models;

namespace MSUStudyChatServer
{
    public class AppSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }
}
