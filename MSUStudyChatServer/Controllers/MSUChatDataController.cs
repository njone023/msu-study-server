﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MSUStudyChatServer.Models;
using MSUStudyChat.Contracts;
using Microsoft.AspNetCore.Cors;

namespace MSUStudyChatServer.Controllers
{
    [Route("msuchatapp")]
    [EnableCors(policyName: "AllowAllOrigins")]
    public class MSUChatDataController : Controller
    {
        private readonly IChatRepository chatRepository;

        public MSUChatDataController(IChatRepository chatRepository)
        {
            this.chatRepository = chatRepository;
        }
     
        [HttpPost("saveuser")]
        public async Task SaveUser([FromBody]User user)
        {
            await this.chatRepository.SaveUser(user);
        }

        [HttpPost("savemessages/{userId}")]
        public async Task<bool> SaveMessages(string userId, [FromBody]List<Message> messages)
        {
            return await this.chatRepository.SaveMessages(userId, messages);
        }

        [HttpPost("savesurveys/{userId}")]
        public async Task<bool> SaveSurveys(string userId, [FromBody]SurveyData surveyData)
        {
            return await this.chatRepository.SaveSurvey(userId, surveyData);
        }

        [HttpPost("timeelapsed/{userId}")]
        public async Task<bool> TimeElapsed(string userId, [FromBody]TimeElapsed timeElapsed)
        {
            return await this.chatRepository.SaveTimeElapsed(userId, timeElapsed);
        }
    }
}
