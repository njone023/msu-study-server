﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MSUStudyChat.Contracts;
using MSUStudyChatServer.Models;

namespace MSUStudyChat.Data
{
    public class ChatRepository : Repository, IChatRepository 
    {
        public ChatRepository(string connectionString) : base(connectionString) { }

        public async Task SaveUser(User user)
        {
      
            await this.ExecuteAsync(Queries.SaveUser, new
            {
                Id = user.userId,
                TestCase = user.testCase
            });       
        }

        public async Task<bool> SaveMessages(string userId, List<Message> messages)
        {
            try
            {
                foreach(var message in messages)
                {
                    await this.ExecuteAsync(Queries.SaveMessage, new
                    {
                        Id = userId, 
                        MessageRound = message.roundNumber,
                        MessageText = message.messageText
                    });
                }

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> SaveSurvey(string userId, SurveyData surveyData)
        {
            try
            {
                foreach(var q in surveyData.surveyQuestions)
                {
                    foreach(var response in q.userResponses)
                    {
                        await this.ExecuteAsync(Queries.SaveSurvey, new
                        {
                            Id = userId,
                            SurveyRound = surveyData.round,
                            SurveyAppendix = q.questionText,
                            SurveyRatingScale = q.ratingScale,
                            AppendixQuestion = response.roundNumber,
                            AppendixQuestionResponse = response.messageText
                        });
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> SaveTimeElapsed(string userId, TimeElapsed timeElapsed)
        {
            try
            {
                await this.ExecuteAsync(Queries.SaveTimeElapsed, new {
                    Id = userId,
                    TimeElapsed = timeElapsed.timeElapsed
                });
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }  
}
