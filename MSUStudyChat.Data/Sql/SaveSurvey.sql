﻿IF @SurveyRound = '1' 
	INSERT INTO dbo.SurveyDataRoundOne (
		StuID,
		SurveyRound,
		SurveyAppendix, 
		SurveyRatingScale,
		AppendixQuestion, 
		AppendixQuestionResponse
	) VALUES (
		@Id,
		@SurveyRound,
		@SurveyAppendix, 
		@SurveyRatingScale,
		@AppendixQuestion,
		@AppendixQuestionResponse
	)
IF @SurveyRound = '2'
INSERT INTO dbo.SurveyDataRoundTwo (
		StuID,
		SurveyRound,
		SurveyAppendix, 
		SurveyRatingScale,
		AppendixQuestion, 
		AppendixQuestionResponse
	) VALUES (
		@Id,
		@SurveyRound,
		@SurveyAppendix, 
		@SurveyRatingScale,
		@AppendixQuestion,
		@AppendixQuestionResponse
)
IF @SurveyRound = '3'
INSERT INTO dbo.SurveyDataRoundThree (
		StuID,
		SurveyRound,
		SurveyAppendix, 
		SurveyRatingScale,
		AppendixQuestion, 
		AppendixQuestionResponse
	) VALUES (
		@Id,
		@SurveyRound,
		@SurveyAppendix, 
		@SurveyRatingScale,
		@AppendixQuestion,
		@AppendixQuestionResponse
)
IF @SurveyRound = 'final survey'
INSERT INTO dbo.SurveyDataFinalRound (
		StuID,
		SurveyRound,
		SurveyAppendix, 
		SurveyRatingScale,
		AppendixQuestion, 
		AppendixQuestionResponse
	) VALUES (
		@Id,
		@SurveyRound,
		@SurveyAppendix, 
		@SurveyRatingScale,
		@AppendixQuestion,
		@AppendixQuestionResponse
)