﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MSUStudyChat.Data {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Queries {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Queries() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MSUStudyChat.Data.Queries", typeof(Queries).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO dbo.UserMessages (
        ///	StuID,
        ///	MessageRound,
        ///	MessageText
        ///) VALUES (
        ///	@Id,
        ///	@MessageRound, 
        ///	@MessageText
        ///).
        /// </summary>
        internal static string SaveMessage {
            get {
                return ResourceManager.GetString("SaveMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IF @SurveyRound = &apos;1&apos; 
        ///	INSERT INTO dbo.SurveyDataRoundOne (
        ///		StuID,
        ///		SurveyRound,
        ///		SurveyAppendix, 
        ///		SurveyRatingScale,
        ///		AppendixQuestion, 
        ///		AppendixQuestionResponse
        ///	) VALUES (
        ///		@Id,
        ///		@SurveyRound,
        ///		@SurveyAppendix, 
        ///		@SurveyRatingScale,
        ///		@AppendixQuestion,
        ///		@AppendixQuestionResponse
        ///	)
        ///IF @SurveyRound = &apos;2&apos;
        ///INSERT INTO dbo.SurveyDataRoundTwo (
        ///		StuID,
        ///		SurveyRound,
        ///		SurveyAppendix, 
        ///		SurveyRatingScale,
        ///		AppendixQuestion, 
        ///		AppendixQuestionResponse
        ///	) VALUES (
        ///		 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string SaveSurvey {
            get {
                return ResourceManager.GetString("SaveSurvey", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UPDATE dbo.StudyUsers
        ///SET TimeSpent = @TimeElapsed
        ///WHERE StuID = @Id
        ///.
        /// </summary>
        internal static string SaveTimeElapsed {
            get {
                return ResourceManager.GetString("SaveTimeElapsed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO dbo.StudyUsers (
        ///           StuID
        ///          ,TestCase
        ///		  )
        ///     VALUES
        ///           (@Id,
        ///			@TestCase
        ///).
        /// </summary>
        internal static string SaveUser {
            get {
                return ResourceManager.GetString("SaveUser", resourceCulture);
            }
        }
    }
}
