using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace MSUStudyChat.Data
{
    public abstract class Repository
    {
        #region Fields

        protected static readonly DateTime SQL_MIN_DATE = DateTime.Parse("1900/01/01");
        protected static readonly DateTime SQL_MAX_DATE = DateTime.Parse("2999/01/01");

        #endregion

        #region Constructor

        public Repository(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        #endregion

        #region Properties

        protected string ConnectionString { get; set; }

        #endregion

        #region Methods

        protected async Task<SqlConnection> GetConnection()
        {
            var conn = new SqlConnection(this.ConnectionString);

            await conn.OpenAsync().ConfigureAwait(false);

            return conn;
        }

        protected async Task<List<T>> QueryAsync<T>(string sql, object parameters = null, int timeout = 60)
        {
            using (var db = await this.GetConnection())
            { 
     
                return (await db.QueryAsync<T>(sql, parameters, commandTimeout: timeout)).ToList();
            }
        }

        protected async Task<T> QueryFirstOrDefaultAsync<T>(string sql, object parameters = null, int timeout = 60)
        {
            using (var db = await this.GetConnection())
            {         
                return await db.QueryFirstOrDefaultAsync<T>(sql, parameters, commandTimeout: timeout);
            }
        }

        protected async Task ExecuteAsync(string query, object parameters)
        {
            using (var conn = await this.GetConnection())
            {

                try
                {
                    await conn.ExecuteAsync(query, parameters);
                }
                catch (Exception ex)
                {

                    throw;
                }       
               
            }
        }

        protected async Task ExecuteProcedureAsync(string query, object parameters)
        {
            using (var conn = await this.GetConnection())
            {
                await conn.ExecuteAsync(query, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        protected async Task<T> QueryProcedureFirstOrDefaultAsync<T>(string query, object parameters)
        {
            using (var conn = await this.GetConnection())
            {
                return await conn.QueryFirstOrDefaultAsync<T>(query, parameters, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion
    }
}

