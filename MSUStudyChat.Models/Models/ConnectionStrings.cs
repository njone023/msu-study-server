﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSUStudyChatServer.Models
{
    public class ConnectionStrings
    {
        public string MSUDataBase { get; set; }
    }
}
