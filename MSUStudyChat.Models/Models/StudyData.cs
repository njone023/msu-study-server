﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSUStudyChatServer.Models
{
    public class StudyData
    {
        public string userEmail { get; set; }
        public string userId { get; set; }
        public List<RoundData> allRoundData { get; set; }
    }

    public class RoundData
    {
        public Message roundMessage { get; set; }
        public SurveyData surveyData { get; set; }
    }

    public class Message
    {
        public string roundNumber { get; set; }
        public string messageText { get; set; }
    }

    public class SurveyData
    {
        public string round { get; set; }
        public List<Question> surveyQuestions { get; set; }
    }

    public class Question
    {
        public string questionText { get; set; }
        public string ratingScale { get; set; }
        public List<Message> userResponses { get; set; }  
    }

    public class User
    {
        public string userId { get; set; }
        public string testCase { get; set; }
    }

    public class TimeElapsed
    {
        public string timeElapsed { get; set; }
    }
}
